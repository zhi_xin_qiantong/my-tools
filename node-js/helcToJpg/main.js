/**
 * 作者：致心 vue项目所有文件命名全部采用中划线分隔   main
 * 创建时间：2021/2/19 12:54
 * https://github.com/catdad-experiments/heic-convert
 */
const {promisify} = require('util');
const fs = require('fs');
const convert = require('heic-convert');
const fileUils = require('../common/file-utils')
const {filesList} = fileUils.readFileList('D:\\window\\桌面\\手机照片');
(async () => {
    for (const i of filesList) {
        if (i.endsWith('.HEIC')) {
            console.log(`正在转换的文件：${i}`);
            const inputBuffer = await promisify(fs.readFile)(i);
            const outputBuffer = await convert({
                buffer: inputBuffer, // the HEIC file buffer
                format: 'JPEG',      // output format
                quality: 1           // the jpeg compression quality, between 0 and 1
            });
            await promisify(fs.writeFile)(i.replace('.HEIC', '.jpg'), outputBuffer);
            console.error(`文件转换完成：${i.replace('.HEIC', '.jpg')}`);
            console.log(`删除源文件：${i}`);
            fileUils.delFile(i, null) // 删除源文件
            console.error(`删除完成`);
        }
    }
})();


