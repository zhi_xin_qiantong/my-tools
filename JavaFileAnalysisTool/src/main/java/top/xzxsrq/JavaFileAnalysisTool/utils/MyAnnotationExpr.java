package top.xzxsrq.JavaFileAnalysisTool.utils;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;

import java.text.MessageFormat;
import java.util.List;

/**
 * @program: MyUtils
 * @description: 获取注解上的值
 * @author: 致心
 * @create: 2021-03-11 17:59
 **/
public abstract class MyAnnotationExpr {
    /**
     * @param annotationExpr 传入注解
     * @param parameterName  传入获取注解上有的参数，如果没有这个参数就会返回第一个参数的值
     * @return
     */
    public static String getValueByParameter(AnnotationExpr annotationExpr, String parameterName) {
        Expression expression = getParamater(annotationExpr, parameterName);
        if (expression == null) {
            List<Expression> children = annotationExpr.getChildNodesByType(Expression.class);
            if (!children.isEmpty()) {
                expression = children.get(0);
            }
        }
        if (expression == null) {
            return null;
        }
        return expression.toString();
    }

    public static Expression getParamater(AnnotationExpr annotationExpr, String parameterName) {
        List<MemberValuePair> children = annotationExpr.getChildNodesByType(MemberValuePair.class);
        for (MemberValuePair memberValuePair : children) {
            if (parameterName.equals(memberValuePair.getNameAsString())) {
                return memberValuePair.getValue();
            }
        }
        return null;
    }

    public static void removeParamater(AnnotationExpr annotationExpr, String name) {
        List<MemberValuePair> children = annotationExpr.getChildNodesByType(MemberValuePair.class);
        for (MemberValuePair memberValuePair : children) {
            if (name.equals(memberValuePair.getNameAsString())) {
                annotationExpr.remove(memberValuePair);
                break;
            }
        }
    }

    /**
     * 如果原来位置有值就会覆盖
     * n.remove(annotationExpr); 移除原来的注解
     * n.addAnnotation(converter1); 添加现在的注解
     *
     * @param annotationExpr
     * @param name
     * @param value
     * @return
     */
    public static AnnotationExpr addParamater(AnnotationExpr annotationExpr, String name, String value) {
        if (getParamater(annotationExpr, name) != null) {
            removeParamater(annotationExpr, name);
        }
        String format = MessageFormat.format("{0} = {1}", name, value);
        StringBuilder sb = new StringBuilder(annotationExpr.toString());
        if (sb.charAt(sb.length() - 1) == ')') { // 最后是一个反括号
            if (sb.charAt(sb.length() - 2) == '(') { // 倒数第二个是正括号
                sb.insert(sb.length() - 1, format);
            } else {
                sb.insert(sb.length() - 1, ", " + format);
            }
        } else {
            sb.append("(")
                    .append(format)
                    .append(")");
        }
        return StaticJavaParser.parseAnnotation(sb.toString());
    }

}
