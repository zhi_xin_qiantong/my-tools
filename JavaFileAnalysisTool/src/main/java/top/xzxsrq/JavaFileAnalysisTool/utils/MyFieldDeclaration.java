package top.xzxsrq.JavaFileAnalysisTool.utils;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.type.Type;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * @program: MyUtils
 * @create: 2021-09-23 17:15
 **/
public abstract class MyFieldDeclaration {
    public static String getTypeName(FieldDeclaration n) {
        Type elementType = n.getElementType();
        return elementType.asString();
    }

    public static String getName(FieldDeclaration n) {
        List<Node> childNodes = n.getChildNodes();
        if (CollectionUtils.isNotEmpty(childNodes)) {
            List<Node> childNodes1 = childNodes.get(childNodes.size() - 1).getChildNodes();
            if (CollectionUtils.isNotEmpty(childNodes1)) {
                return childNodes1.get(childNodes1.size() - 1).toString();
            }
        }
        return null;
    }
}
