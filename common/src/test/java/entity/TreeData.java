package entity;

import lombok.Data;

import java.util.List;

/**
 * @program: MyUtils
 * @create: 2021-12-01 09:45
 **/
@Data
public class TreeData {
    private String name;
    private List<TreeData> children;
}
