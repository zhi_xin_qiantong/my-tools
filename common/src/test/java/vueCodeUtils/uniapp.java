package vueCodeUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import top.xzxsrq.common.utils.FileUtilsZX;
import top.xzxsrq.common.utils.ObjectUtilsZX;
import top.xzxsrq.common.utils.StringUtilsZX;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * @program: MyUtils
 * @create: 2022-01-17 10:15
 **/
public class uniapp {
    private static class Config {
        static String pageJson = "C:\\CODE\\nodeJS\\zhengBang\\boarManageUniapp\\pages.json";
        static String uniIdeaConfig = "uniappIdeaConfig.js";
        static String projectBase = "";

        static {
            File file = new File(pageJson);
            projectBase = file.getParentFile().getAbsolutePath();
            uniIdeaConfig = projectBase + File.separator + uniIdeaConfig;
        }
    }

    @Test
    public void parsePageJsonToAddCom() throws IOException {
        PrintStream ps = new PrintStream(new FileOutputStream(Config.uniIdeaConfig));
        System.setOut(ps);
        String pageJsonFile = FileUtilsZX.readFileToString(new File(Config.pageJson), "utf8");
        JSONObject pageJson = JSON.parseObject(pageJsonFile);
        JSONObject easycom = pageJson.getJSONObject("easycom");
        if (ObjectUtilsZX.isEmpty(easycom)) {
            return;
        }
        JSONObject custom = easycom.getJSONObject("custom");
        BiConsumer<String, Object> func = (key, value) -> {
            String s = value.toString();
            StringBuilder dir = new StringBuilder(Config.projectBase);
            StringBuilder libPath = new StringBuilder();
            if (s.startsWith("@/")) {
                s = s.replace("@/", "");
                libPath.append("@/");
                String[] split = s.split("/");
                for (int i = 0; i < split.length - 2; i++) {
                    dir.append("/").append(split[i]);
                    libPath.append(split[i]).append("/");
                }
            } else {
                dir.append("/node_modules/");
                String[] split = s.split("/");
                for (int i = 0; i < split.length - 2; i++) {
                    dir.append("/").append(split[i]);
                    libPath.append(split[i]).append("/");
                }
            }
            Iterator<File> fileIterator = FileUtilsZX.iterateFiles(new File(String.valueOf(dir)), new String[]{"vue"}, true);
            List<String> imports = new LinkedList<>();
            List<String> components = new LinkedList<>();
            while (fileIterator.hasNext()) {
                File file = fileIterator.next();
                String fileName = file.getName().replace(".vue", "");
                String parentName = file.getParentFile().getName();
                if (fileName.equals(parentName) && fileName.matches(key)) {
                    s = StringUtilsZX.toCamelCase(fileName);
                    imports.add("import " + s + " from '" + libPath + fileName + "/" + fileName + ".vue'"
                            .replace("@/node_modules/", "")
                            .replaceAll("\\\\", "/"));
                    components.add("Vue.component('" + fileName + "', " + s + ")");
                }
            }
            System.out.println("// " + value);
            imports.forEach(System.out::println);
            components.forEach(System.out::println);
            System.out.println();
        };
        if (ObjectUtilsZX.isEmpty(custom)) {
            easycom.forEach(func);
        } else {
            custom.forEach(func);
        }
    }

    @Test
    public void addComponents() {
        String projectBasePath = "C:\\CODE\\nodeJS\\project\\uni-app-base";
        String name = "node_modules\\uview-ui\\components";
        name = name.replaceAll("\\\\", "/");
        String basePath = projectBasePath + File.separator + name;
        Iterator<File> fileIterator = FileUtilsZX.iterateFiles(new File(basePath), new String[]{"vue"}, true);
        List<String> imports = new LinkedList<>();
        List<String> components = new LinkedList<>();
        while (fileIterator.hasNext()) {
            File file = fileIterator.next();
            String fileName = file.getName().replace(".vue", "");
            String parentName = file.getParentFile().getName();
            if (fileName.equals(parentName)) {
                String s = StringUtilsZX.toCamelCase(fileName);
                imports.add("import " + s + " from '@/" + name + "/" + fileName + "/" + fileName + ".vue'".replace("@/node_modules/", ""));
                components.add("Vue.component('" + fileName + "', " + s + ")");
            }
        }
        imports.forEach(System.out::println);
        components.forEach(System.out::println);
    }

    /**
     * 生成调用参数参数
     */
    @Test
    public void genUniConfigCode() {
        String config = "";
        String[] row = config.split("\n");
        for (String s : row) {
            String[] col = s.split("\t");
            String defaultValue = "";
            if ("String".equals(col[1])) {
                if (col[3].contains("默认为")) {
                    defaultValue = col[3].substring(col[3].lastIndexOf("默认为") + 3).replaceAll("\"","`");
                } else {
                    defaultValue = "``";
                }
            } else if ("Boolean".equals(col[1])) {
                if ("否".equals(col[2])) {
                    defaultValue = "false";
                } else {
                    defaultValue = "true";
                }
            } else if ("Function".equals(col[1])) {
                defaultValue = "(" + col[0].trim() + ") => {}";
            }
            System.out.println(col[0].trim()+" = "+defaultValue+", // " + col[3]);
        }
    }

    /**
     * 回调参数生成, 没有默认值
     */
    @Test
    public void genCallFuncCode() {
        String config = "id\tString\t节点的 ID\n" +
                "dataset\tObject\t节点的 dataset\n" +
                "left\tNumber\t节点的左边界坐标\n" +
                "right\tNumber\t节点的右边界坐标\n" +
                "top\tNumber\t节点的上边界坐标\n" +
                "bottom\tNumber\t节点的下边界坐标\n" +
                "width\tNumber\t节点的宽度\n" +
                "height\tNumber\t节点的高度";
        String[] row = config.split("\n");
        for (String s : row) {
            String[] col = s.split("\t");
            String defaultValue = "undefined";
            if ("Function".equals(col[1])) {
                defaultValue = "(" + col[0].trim() + ") => {}";
            }
            System.out.println(col[0].trim()+" : "+defaultValue+", // " + col[2]);
        }
    }
}
