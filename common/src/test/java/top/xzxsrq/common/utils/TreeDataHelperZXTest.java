package top.xzxsrq.common.utils;

import entity.TreeData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class TreeDataHelperZXTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private TreeData getItem(int name) {
        TreeData item = new TreeData();
        item.setName(name + "");
        TreeData children1 = new TreeData();
        children1.setName(name + "-1");
        TreeData children2 = new TreeData();
        children2.setName(name + "-2");
        List<TreeData> getChildren1 = new LinkedList<>();
        getChildren1.add(children1);
        getChildren1.add(children2);
        TreeData children21 = new TreeData();
        children21.setName((name*100 + 1) + "-1");
        TreeData children22 = new TreeData();
        children22.setName((name*100 + 1) + "-2");
        List<TreeData> getChildren2 = new LinkedList<>();
        getChildren2.add(children21);
        getChildren2.add(children22);
        getChildren1.addAll(getChildren2);
        item.setChildren(getChildren1);
        return item;
    }

    @Test
    public void eachTreeList() {
        List<TreeData> list = new LinkedList<>();
        list.add(getItem(1));
        list.add(getItem(2));
        list.add(getItem(3));
        for (TreeData treeData : list) {
            System.out.println("treeData = " + treeData);
        }
        TreeDataHelperZX.eachTreeList(list, "children", (item, path) -> {
            System.out.println("item = " + item);
            System.out.println("path = " + path);
        });
    }

    @Test
    public void getParent() {
        List<TreeData> list = new LinkedList<>();
        list.add(getItem(1));
        list.add(getItem(2));
        list.add(getItem(3));
        for (TreeData treeData : list) {
            System.out.println("treeData = " + treeData);
        }
        TreeData source = new TreeData();
        source.setName("3-1");
        List<TreeData> parent = TreeDataHelperZX.getParent(list, source, true, "name", "children");
        System.out.println("parent = " + parent);
    }
}
