package top.xzxsrq.common.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class DistinctKeyUtilZXTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void distinctByKey() {
        List<Integer> list1 = Arrays.asList(1, 1, 2, 3, 4, 5, 6, 6, 7, 8, 8, 6, 9, 10);
        List<Integer> list3 = list1
                .stream()
                .filter(DistinctKeyUtilZX.distinctByKey(n -> n))
                .collect(Collectors.toList());
        System.out.println("list3 = " + list3);
    }
    @Test
    public void distinctByKeyS() {
        class test {
            final int field1;
            final int field2;

            public test(int field1, int field2) {
                this.field1 = field1;
                this.field2 = field2;
            }

            @Override
            public String toString() {
                return "test{" +
                        "field1=" + field1 +
                        ", field2=" + field2 +
                        '}';
            }
        }

        List<test> list1 = Arrays.asList(
                new test(0,1),
                new test(0,2),
                new test(0,3),
                new test(0,1),
                new test(0,2),
                new test(0,3)
        );
        List<test> list3 = list1
                .stream()
                .filter(DistinctKeyUtilZX.distinctByKey(n -> n.field1 + n.field2))
                .collect(Collectors.toList());
        System.out.println("list3 = " + list3);
    }
}
