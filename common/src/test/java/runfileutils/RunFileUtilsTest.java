package runfileutils;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import top.xzxsrq.common.io.filefilter.DirectoryFileFilterZX;
import top.xzxsrq.common.io.filefilter.FileFileFilterZX;
import top.xzxsrq.common.utils.FileUtilsZX;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Function;

/**
 * 文件重命名
 */
public class RunFileUtilsTest {
    /**
     * 文件夹和文件移除空格包括子项
     */
    @Test
    public void removeSpaceByFileAndDirAndSub() {
        Function<File, Void> rename = file -> {
            String absolutePath = file.getAbsolutePath();
            String parent = file.getParent();
            String baseName = FilenameUtils.getName(absolutePath);
            if (baseName.contains(" ")) {
                System.out.println(absolutePath);
            }
            baseName = baseName.replaceAll(" ", "");
            String newName = FilenameUtils.concat(parent, baseName);
            try {
                FileUtilsZX.rename(file, newName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        };
        Iterator<File> fileIterator = FileUtilsZX.iterateFilesAndDirs(
                new File(
                        "E:\\window\\desktop\\test"
                ), new FileFileFilterZX(), new DirectoryFileFilterZX());
        final Collection<File> dirs = new java.util.LinkedList<>();
        while (fileIterator.hasNext()) {
            File file = fileIterator.next();
            if (file.isFile()) {
                rename.apply(file);
            } else {
                dirs.add(file);
            }
        }
        for (File file : dirs) {
            rename.apply(file);
        }
    }

    /**
     * 删除文件夹下面后缀名相同的文件包括子项
     */
    @Test
    public void deleteSuffixByFileAndDirAndSub() {
        Iterator<File> fileIterator = FileUtilsZX.iterateFiles(new File(
                "D:\\CODE\\nodejs\\project\\zhixin-blog-vite\\docs\\.vuepress\\vitePlugins"
        ), new String[]{"js"}, true);
        while (fileIterator.hasNext()) {
            File next = fileIterator.next();
            FileUtilsZX.deleteFile(next.getAbsolutePath());
        }
    }

    /**
     * 删除后缀名同名的另一个后缀名文件
     */
    @Test
    public void deleteAnotherSuffixFileWithTheSameSuffixName() {
        String origin = "ts";
        String del = "js";
        Iterator<File> fileIterator = FileUtilsZX.iterateFiles(new File(
                "D:\\CODE\\nodejs\\project\\vue2-learning\\src"
        ), new String[]{origin}, true);
        while (fileIterator.hasNext()) {
            File next = fileIterator.next();
            String name = FilenameUtils.getBaseName(next.getAbsolutePath());
            String s = next.getParent() + File.separator + name + "." + del;
            System.out.println("删除的" + del + "文件：" + s);
            FileUtilsZX.deleteFile(s);
        }
    }
}
