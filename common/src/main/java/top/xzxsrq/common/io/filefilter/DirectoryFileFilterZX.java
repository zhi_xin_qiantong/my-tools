package top.xzxsrq.common.io.filefilter;

import org.apache.commons.io.filefilter.DirectoryFileFilter;

import java.io.File;
import java.util.function.Function;

public class DirectoryFileFilterZX extends DirectoryFileFilter {
    private final Function<File, Boolean> callBack;

    public DirectoryFileFilterZX(Function<File, Boolean> callBack) {
        this.callBack = callBack;
    }

    public DirectoryFileFilterZX() {
        this.callBack = null;
    }

    @Override
    public boolean accept(File dir) {
        if (super.accept(dir)) {
            if (this.callBack == null) {
                return true;
            }
            return this.callBack.apply(dir);
        }
        return true;
    }
}
