package top.xzxsrq.common.io.filefilter;

import org.apache.commons.io.filefilter.FileFileFilter;

import java.io.File;
import java.util.function.Function;

public class FileFileFilterZX extends FileFileFilter {
    private final Function<File, Boolean> callBack;

    public FileFileFilterZX(Function<File, Boolean> callBack) {
        this.callBack = callBack;
    }

    public FileFileFilterZX() {
        this.callBack = null;
    }

    @Override
    public boolean accept(File file) {
        if (super.accept(file)) {
            if (this.callBack == null) {
                return true;
            }
            return this.callBack.apply(file);
        }
        return true;
    }
}
