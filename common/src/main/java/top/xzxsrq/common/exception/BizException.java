package top.xzxsrq.common.exception;

/**
 * @program: MyUtils
 * @create: 2021-09-22 13:15
 **/
public class BizException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BizException() {
    }

    public BizException(String msg) {
        super(msg);
    }
}