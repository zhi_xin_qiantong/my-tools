package top.xzxsrq.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 创建新文件和目录
 *
 * @author Fly
 */
@Slf4j
public abstract class FileUtilsZX extends FileUtils {

    /**
     * 验证字符串是否为正确路径名的正则表达式
     */
    private static String matches = "[A-Za-z]:\\\\[^:?\"><*]*";

    /**
     * 根据路径删除指定的目录或文件，无论存在与否
     *
     * @param deletePath
     * @return
     */
    public static boolean deleteFolder(String deletePath) {
        if (deletePath.matches(matches)) {
            File file = new File(deletePath);
            // 判断目录或文件是否存在
            if (!file.exists()) {
                // 不存在返回 true
                System.out.println("deletePath文件不存在 = " + deletePath);
                return true;
            } else {
                // 判断是否为文件
                if (file.isFile()) {
                    // 为文件时调用删除文件方法
                    return deleteFile(deletePath);
                } else {
                    // 为目录时调用删除目录方法
                    return deleteDirectory(deletePath);
                }
            }
        } else {
            System.out.println("要传入正确路径！");
            return true;
        }
    }

    /**
     * 删除单个文件
     *
     * @param filePath 文件路径
     * @return
     */
    public static boolean deleteFile(String filePath) {
        File file = new File(filePath);
        // 路径为文件且不为空则进行删除
        if (file.exists()) {
            if (file.isFile()) {
                return file.delete();
            } else {
                System.out.println("filePath不是文件 = " + filePath);
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 删除目录（文件夹）以及目录下的文件
     *
     * @param dirPath
     * @return
     */
    public static boolean deleteDirectory(String dirPath) {
        // 如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!dirPath.endsWith(File.separator)) {
            dirPath = dirPath + File.separator;
        }
        File dirFile = new File(dirPath);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        // 获得传入路径下的所有文件
        File[] files = dirFile.listFiles();
        // 循环遍历删除文件夹下的所有文件(包括子目录)
        if (files != null) {
            for (File file1 : files) {
                if (file1.isFile()) {
                    // 删除子文件
                    flag = deleteFile(file1.getAbsolutePath());
                    System.out.println("删除成功 = " + file1.getAbsolutePath());
                    if (!flag) {
                        break;// 如果删除失败，则跳出
                    }
                } else {// 运用递归，删除子目录
                    flag = deleteDirectory(file1.getAbsolutePath());
                    if (!flag) {
                        break;// 如果删除失败，则跳出
                    }
                }
            }
        }

        if (!flag) {
            return false;
        }
        // 删除当前目录
        return dirFile.delete();
    }

    /**
     * 创建单个文件
     *
     * @param filePath 文件所存放的路径
     * @return
     */
    public static File createFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {// 判断文件是否存在
            System.out.println("目标文件已存在 = " + filePath);
            return file;
        }
        if (filePath.endsWith(File.separator)) {// 判断文件是否为目录
            System.out.println("目标文件不能为目录！");
            return file;
        }
        if (!file.getParentFile().exists()) {// 判断目标文件所在的目录是否存在
            // 如果目标文件所在的文件夹不存在，则创建父文件夹
            System.out.println("目标文件所在目录不存在，准备创建它！");
            if (!file.getParentFile().mkdirs()) {// 判断创建目录是否成功
                System.out.println("创建目标文件所在的目录失败！");
                return file;
            }
        }
        try {
            if (file.createNewFile()) {// 创建目标文件
                System.out.println("创建文件成功 = " + filePath);
                return file;
            } else {
                System.out.println("创建文件失败！");
                return file;
            }
        } catch (IOException e) {// 捕获异常
            e.printStackTrace();
            System.out.println("创建文件失败 = " + e.getMessage());
            return file;
        }
    }

    /**
     * 创建目录(如果目录存在就删掉目录)
     *
     * @param destDirName 目标目录路径
     * @return
     */
    public static boolean createDir(String destDirName) {
        File dir = new File(destDirName);
        if (dir.exists()) {// 判断目录是否存在
            System.out.println("目标目录已存在 = " + destDirName);
            return true;
        }
        if (!destDirName.endsWith(File.separator)) {// 结尾是否以"/"结束
            destDirName = destDirName + File.separator;
        }
        if (dir.mkdirs()) {// 创建目标目录
            System.out.println("创建目录成功 = " + destDirName);
            return true;
        } else {
            System.out.println("创建目录失败！");
            return false;
        }
    }

    /**
     * 创建临时文件
     *
     * @param prefix  前缀字符串定义的文件名;必须至少有三个字符长
     * @param suffix  后缀字符串定义文件的扩展名;如果为null后缀".tmp" 将被使用
     * @param dirName 该目录中的文件被创建。对于默认的临时文件目录nullis来传递
     * @return 一个抽象路径名新创建的空文件。
     * @throws IllegalArgumentException -- 如果前缀参数包含少于三个字符
     * @throws IOException              -- 如果文件创建失败
     * @throws SecurityException        -- 如果SecurityManager.checkWrite(java.lang.String)方法不允许创建一个文件
     */
    public static String createTempFile(String prefix, String suffix, String dirName) {
        File tempFile;
        if (dirName == null) {// 目录如果为空
            try {
                tempFile = File.createTempFile(prefix, suffix);// 在默认文件夹下创建临时文件
                return tempFile.getCanonicalPath();// 返回临时文件的路径
            } catch (IOException e) {// 捕获异常
                e.printStackTrace();
                System.out.println("创建临时文件失败 = " + e.getMessage());
                return null;
            }
        } else {
            // 指定目录存在
            File dir = new File(dirName);// 创建目录
            if (!dir.exists()) {
                // 如果目录不存在则创建目录
                if (FileUtilsZX.createDir(dirName)) {
                    System.out.println("创建临时文件失败，不能创建临时文件所在的目录！");
                    return null;
                }
            }
            try {
                tempFile = File.createTempFile(prefix, suffix, dir);// 在指定目录下创建临时文件
                return tempFile.getCanonicalPath();// 返回临时文件的路径
            } catch (IOException e) {// 捕获异常
                e.printStackTrace();
                System.out.println("创建临时文件失败 = " + e.getMessage());
                return null;
            }
        }
    }

    /**
     * 不要传递系统的类，作为getAppPath的参数，如java.lang.String.class，当然，也不要传递那些已经位于JDK中的那些类，比如xml相关的一些类等等。
     * 要传递应该是程序中主要的运行类，不要传递程序中的支持类库中的类文件，也就是那些第三方的类库中的类文件，否则得到的将是那些类库的位置。
     * -----------------------------------------------------------------------
     * getAppPath需要一个当前程序使用的Java类的class属性参数，它可以返回打包过的
     * Java可执行文件（jar，war）所处的系统目录名或非打包Java程序所处的目录
     *
     * @param cls 为Class类型
     * @return 返回值为该类所在的Java程序运行的目录
     * -------------------------------------------------------------------------
     */
    public static String getAppPath(Class cls) {
        //检查用户传入的参数是否为空
        if (cls == null) {
            throw new IllegalArgumentException("参数不能为空！");
        }
        ClassLoader loader = cls.getClassLoader();
        //获得类的全名，包括包名
        String clsName = cls.getName() + ".class";
        //获得传入参数所在的包
        Package pack = cls.getPackage();
        StringBuilder path = new StringBuilder();
        //如果不是匿名包，将包名转化为路径
        if (pack != null) {
            String packName = pack.getName();
            //此处简单判定是否是Java基础类库，防止用户传入JDK内置的类库
            if (packName.startsWith("java.") || packName.startsWith("javax.")) {
                throw new IllegalArgumentException("不要传送系统类！");
            }
            //在类的名称中，去掉包名的部分，获得类的文件名
            clsName = clsName.substring(packName.length() + 1);
            //判定包名是否是简单包名，如果是，则直接将包名转换为路径，
            if (!packName.contains(".")) {
                path = new StringBuilder(packName + "/");
            } else {//否则按照包名的组成部分，将包名转换为路径
                int start = 0, end;
                end = packName.indexOf(".");
                while (end != -1) {
                    path.append(packName.substring(start, end)).append("/");
                    start = end + 1;
                    end = packName.indexOf(".", start);
                }
                path.append(packName.substring(start)).append("/");
            }
        }
        //调用ClassLoader的getResource方法，传入包含路径信息的类文件名
        java.net.URL url = loader.getResource(path + clsName);
        //从URL对象中获取路径信息
        String realPath = url.getPath();
        //去掉路径信息中的协议名"file:"
        int pos = realPath.indexOf("file:");
        if (pos > -1) {
            realPath = realPath.substring(pos + 5);
        }
        //去掉路径信息最后包含类文件信息的部分，得到类所在的路径
        pos = realPath.indexOf(path + clsName);
        realPath = realPath.substring(0, pos - 1);
        //如果类文件被打包到JAR等文件中时，去掉对应的JAR等打包文件名
        if (realPath.endsWith("!")) {
            realPath = realPath.substring(0, realPath.lastIndexOf("/"));
        }
      /*------------------------------------------------------------
       ClassLoader的getResource方法使用了utf-8对路径信息进行了编码，当路径
        中存在中文和空格时，他会对这些字符进行转换，这样，得到的往往不是我们想要
        的真实路径，在此，调用了URLDecoder的decode方法进行解码，以便得到原始的
        中文及空格路径
      -------------------------------------------------------------*/
        try {
            realPath = java.net.URLDecoder.decode(realPath, "utf-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // 移除 /
        if (realPath.charAt(0) == '/' && Platform.isWindows()) {
            realPath = realPath.substring(1);
        }
        log.info("app路径为: {}", realPath);
        return realPath;
    }//getAppPath定义结束

    public static String getProjectPath() throws IOException {
        // 当前项目下路径
        File file = new File("");
        String filePath = file.getCanonicalPath();
        log.info("当前项目路径为: {}", filePath);
        return filePath;
    }

    public static boolean exists(File file) {
        return file != null && file.exists();
    }

    public static boolean notExists(File file) {
        return !exists(file);
    }

    public static InputStream getClassPathStream(String path) {
        return ClassLoader.getSystemResourceAsStream(path);
    }

    /**
     * 文件重命名
     *
     * @param file
     * @param newName       绝对路径
     * @param deleteOldFile
     */
    public static boolean rename(File file, String newName, boolean deleteOldFile) throws IOException {
        File oldFile = new File(newName);
        boolean b = file.renameTo(oldFile);
        if (!deleteOldFile) {
            FileUtilsZX.copyFile(oldFile, file);
        }
        return b;
    }

    /**
     * 文件重命名
     *
     * @param file
     * @param newName 绝对路径
     */
    public static boolean rename(File file, String newName) throws IOException {
        return rename(file, newName, true);
    }
}
