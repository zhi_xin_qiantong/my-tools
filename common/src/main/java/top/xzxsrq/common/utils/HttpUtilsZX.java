package top.xzxsrq.common.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 * 包装spring的RestTemlate
 *
 * @program: cmp-service
 * @create: 2021-10-21 09:20
 **/
public abstract class HttpUtilsZX {

    public static String postByJson(String url, Object body) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        String json = JSON.toJSONString(body);
        HttpEntity<String> formEntity = new HttpEntity<>(json, headers);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(url, formEntity, String.class);
    }
}
