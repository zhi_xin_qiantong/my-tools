package top.xzxsrq.common.utils;

import java.util.LinkedList;
import java.util.List;

/**
 * 引用检测
 *
 * @program: MyUtils
 * @create: 2021-12-17 17:12
 **/
public class RefDetZX {
    private List<Object> data = new LinkedList<>();

    public void add(Object value) {
        if (ObjectUtilsZX.isEmpty(value)) {
            return;
        }
        if (ObjectUtilsZX.isBaseType(value)) {
            return;
        }
        data.add(value);
    }

    public boolean contains(Object value) {
        if (ObjectUtilsZX.isEmpty(value)) {
            return false;
        }
        return data.contains(value);
    }
}
