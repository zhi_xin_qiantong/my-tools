package top.xzxsrq.common.utils;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * @program: MyUtils
 * @create: 2021-09-24 20:35
 **/
public abstract class NumberUtilsZX extends NumberUtils {

    /**
     * 补零函数
     *
     * @param value
     * @param length
     * @return
     */
    public static String doHandleZero(String value, int length) {
        if (value.length() >= length) {
            return value;
        }
        int i = length - value.length();
        String repeat = StringUtils.repeat('0', i);
        return repeat + value;
    }

    /**
     * 补零函数
     *
     * @param zero
     * @returns
     */
    public static String doHandleZero(int zero, int length) {
        return doHandleZero(String.valueOf(zero), length);
    }

    /**
     * 元转万元
     *
     * @param b
     * @return
     */
    public static BigDecimal tenThousandTransfer(BigDecimal b) {
        if (null == b || BigDecimal.ZERO.compareTo(b) == 0) {
            return BigDecimal.ZERO;
        } else {
            return b.multiply(new BigDecimal("0.0001")).setScale(2, RoundingMode.HALF_UP);
        }
    }

    public static BigDecimal mul(BigDecimal value, int i) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        return value.multiply(new BigDecimal(i));
    }

    /**
     * 格式化千分符
     * 10000 => "10,000"
     * @param num
     * @return
     */
    public static String toThousandFilter(String num) {
        if (num == null) {
            return null;
        }
        // 判断是否有小数
        int index = num.indexOf(".");
        if (index >= 0) {
            String integer = num.substring(0, index);
            String decimal = num.substring(index);
            // 分隔后的整数+小数拼接起来
            return addSeparator(integer) + decimal;
        } else {
            return addSeparator(num);
        }
    }

    // 添加分隔符
    public static String addSeparator(String num) {
        int length = num.length();
        ArrayList list = new ArrayList();
        while (length > 3) {
            list.add(num.substring(length - 3, length));
            length = length - 3;
        }
        // 将前面小于三位的数字添加到ArrayList中
        list.add(num.substring(0, length));
        StringBuffer buffer = new StringBuffer();
        // 倒序拼接
        for (int i = list.size() - 1; i > 0; i--) {
            buffer.append(list.get(i) + ",");
        }
        buffer.append(list.get(0));
        return buffer.toString();
    }

    public static BigDecimal getBigDecimal(String s) {
        if (StringUtils.isEmpty(s)) {
            return null;
        }
        return new BigDecimal(s);
    }

    public static BigDecimal getBigDecimal(BigDecimal totalArea) {
        if (ObjectUtils.isEmpty(totalArea)) {
            return null;
        }
        return totalArea;
    }

    public static BigDecimal getBigDecimal(Integer totalHousingNumber) {
        if (ObjectUtils.isEmpty(totalHousingNumber)) {
            return null;
        }
        return new BigDecimal(totalHousingNumber);
    }

    /**
     * 四舍五入 保留两位小数
     * @param number
     * @return
     */
    public static BigDecimal scale2HalfUp(Number number){
        return scaleHalfUp(number,2);
    }

    /**
     * 四舍五入 保留小数
     * @param number
     * @param scale
     * @return
     */
    public static BigDecimal scaleHalfUp(Number number,int scale){
        BigDecimal bigDecimal = new BigDecimal(number.toString()).setScale(scale, RoundingMode.HALF_UP);
        NumberFormat instance = NumberFormat.getInstance();
        String format = instance.format(bigDecimal);
        return new BigDecimal(format);
    }
}
