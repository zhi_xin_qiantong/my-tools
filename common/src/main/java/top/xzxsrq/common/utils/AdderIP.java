package top.xzxsrq.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 
 * @name AdderIP.java
 * @package_name zhixin.util
 * @project_name webproject
 * @author 致心
 * @date 2019年4月17日   下午5:10:07
 * @funtion:
 */
@Slf4j
public abstract class AdderIP {

	 public static String getIPV4(){
		 InetAddress addr = null;
		 try {
			 addr = InetAddress.getLocalHost();
		 } catch (UnknownHostException e) {
			 e.printStackTrace();
		 }
		 log.info("IP地址：{}，主机名：{}", addr.getHostAddress(), addr.getHostName());
		 return addr.getHostAddress();
	 }

}

