package top.xzxsrq.common.utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @program: MyUtils
 * @create: 2021-07-02 14:09
 **/
public abstract class PathLink {
    /**
     * 文件相对路径生成
     *
     * @param source
     * @param target
     * @return
     */
    public static String fileToFile(File source, File target) {
        return fileToFile(source.getAbsolutePath(), target.getAbsolutePath());
    }

    /**
     * 文件相对路径生成
     *
     * @param source
     * @param target
     * @return
     */
    public static String fileToFile(String source, String target) {
        StringBuilder result = new StringBuilder();
        String separator;
        if (Platform.isWindows()) {
            separator = File.separator + File.separator;
        } else {
            separator = File.separator;
        }
        String[] sourcePathSplit = source.split(separator);
        String[] targetPathSplit = target.split(separator);
        int notEquals = 0;
        for (int i = 0; i < targetPathSplit.length; i++) {
            String item = targetPathSplit[i];
            if (i >= sourcePathSplit.length || !item.equals(sourcePathSplit[i])) {
                if (notEquals == 0) {
                    notEquals = i;
                }
                result.append("..").append(File.separator);
            }
        }
        for (int i = notEquals; i < sourcePathSplit.length; i++) {
            result.append(sourcePathSplit[i]).append(File.separator);
        }
        return result.substring(result.indexOf(File.separator) + 1, result.length() - 1);
    }

    /**
     * 获取resources文件夹下的文件流
     * jsonToExcel/模板.xlsx 前面不带/
     *
     * @param path
     * @return
     */
    public static InputStream getResourcesInputStream(String path) {
        return PathLink.class.getClassLoader().getResourceAsStream(path);
    }

    public static File getResourcesFile(String path) {
        try {
            URL resource = PathLink.class.getClassLoader().getResource(path);
            if (resource == null) {
                return null;
            }
            return new File(resource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getResourcesString(String path, Charset encoding) {
        try {
            if (ObjectUtilsZX.isEmpty(encoding)) {
                encoding = StandardCharsets.UTF_8;
            }
            return IOUtils.toString(getResourcesInputStream(path), encoding);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String s = fileToFile("D:\\window\\桌面\\md\\imgs\\java笔记620.png", "D:\\window\\桌面\\md\\1. JAVA----多看文档\\1.3. JDK，JRE，JVM的区别和关系.md");
        System.out.println(s);
    }
}
