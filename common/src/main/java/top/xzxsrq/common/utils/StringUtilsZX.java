package top.xzxsrq.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: MyUtils
 * @description: 命名转换
 * @author: 致心
 * @create: 2021-03-12 10:48
 **/
public abstract class StringUtilsZX extends StringUtils {

    /**
     * @param value
     * @return
     * @Description :map 的 key ADD_ADD -->addAdd
     * Map<String,Object> test=new HashMap<>();
     * test.put("ADD_ADD","ssssssss");
     * test.put("ADD_BDD","ssssssss");
     * test.put("ADD_CDD","ssssssss");
     * test.put("ADD_DDD","ssssssss");
     * test.put("BDD_DDD","ssssssss");
     * test.put("CDD_DDD","ssssssss");
     * test.put("DDD_DDD","ssssssss");
     * Map<String,Object> test2=new HashMap<>();
     * test2.put("ADD_ADD","ssssssss");
     * test2.put("ADD_BDD","ssssssss");
     * test2.put("ADD_CDD","ssssssss");
     * test2.put("ADD_DDD",test);
     * {addBdd=ssssssss, addAdd=ssssssss, addDdd={dddDdd=ssssssss, addBdd=ssssssss, addAdd=ssssssss, addDdd=ssssssss, addCdd=ssssssss, bddDdd=ssssssss, cddDdd=ssssssss}, addCdd=ssssssss}
     * @Author :致心
     * @Date :2021/3/12 14:04
     */
    public static Map<String, Object> mapBigLineKeyToSmallHump(Map<String, Object> value) {
        Map<String, Object> loop = new HashMap<>();
        return mapBigLineKeyToSmallHump(value, loop, 1);
    }

    private static Map<String, Object> mapBigLineKeyToSmallHump(Map<String, Object> value, Map<String, Object> loop, int count) {
        loop.put("loop" + count, value);
        List<String> delKey = new ArrayList<>(value.keySet());
        if (count != 1 && value.equals(loop.get("loop1"))) {
            return value;
        }
        for (String s : delKey) {
            Object temp = value.get(s);
            value.remove(s);
            if (temp instanceof Map) {
                value.put(littleHumpSplit(s, "_", false), mapBigLineKeyToSmallHump((Map<String, Object>) temp, loop, count + 1));
            } else {
                value.put(littleHumpSplit(s, "_", false), temp);
            }
        }
        return value;
    }


    /**
     * @param value  传入要转换的值
     * @param split  分隔符 没有可以传入null，这使用默认的下划线：_
     * @param splice 是否进行拼接
     * @return
     * @Description :ADD_DDDDD-->add_ddddd(拼接) ADD_DDDDD-->addddddd(不拼接)
     * @Author :致心
     * @Date :2021/3/12 10:50
     */
    public static String lowerCaseSplit(String value, String split, Boolean splice) {
        if (split == null) {
            split = "_";
        }
        String[] items = value.split(split);
        for (int i = 0; i < items.length; i++) {
            items[i] = items[i].toLowerCase();
        }
        if (!splice) {
            return String.join("", items);
        }
        return String.join(split, items);
    }

    /**
     * @param value  传入要转换的值
     * @param split  分隔符 没有可以传入null，这使用默认的下划线：_
     * @param splice 是否进行拼接
     * @return
     * @Description :ADD_DDDDD-->addDdddd(不拼接) ADD_DDDDD-->add_dddd(拼接)
     * @Author :致心
     * @Date :2021/3/12 11:03
     */
    public static String littleHumpSplit(String value, String split, Boolean splice) {
        if (split == null) {
            split = "_";
        }
        String[] items = value.split(split);
        for (int i = 0; i < items.length; i++) {
            if (i == 0) {
                items[i] = items[i].toLowerCase();
            } else {
                items[i] = captureName(items[i].toLowerCase());
            }
        }
        if (splice) {
            return String.join(split, items);
        }
        return String.join("", items);
    }

    /**
     * @param value 传入要转换的值
     * @param split 分隔符 没有可以传入null，这使用默认的下划线：_
     * @return
     * @Description :AddDddd-->Add_Dddd
     * @Author :致心
     * @Date :2021/3/12 11:19
     */
    public static String bigCamelCaseToCustomSeparator(String value, String split) {
        if (split == null) {
            split = "_";
        }
        value = value.replaceAll("([A-Z])", split + "$1");
        return value.substring(1);
    }

    /**
     * @param value 传入要转换的值
     * @return
     * @Description :AddDddd-->addDddd
     * @Author :致心
     * @Date :2021/3/12 11:51
     */
    public static String bigHumpToSmallHump(String value) {
        return firstLetterToLowercase(value);
    }

    /**
     * @param value
     * @return
     * @Description :Sys-Job-Log-Controller --> sys-job-log-controller
     * @Author :致心
     * @Date :2021/3/12 17:59
     */
    public static String wireSeparationToLowercase(String value) {
        value = firstLetterToLowercase(value);
        String[] items = value.split("-");
        for (int i = 0; i < items.length; i++) {
            items[i] = firstLetterToLowercase(items[i]);
        }
        return String.join("-", items);
    }

    /**
     * 将字符串的首字母转大写
     *
     * @param str 需要转换的字符串
     * @return
     */
    public static String captureName(String str) {
        // 进行字母的ascii编码前移，效率要高于截取字符串进行转换的操作
        char[] chars = str.toCharArray();
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            chars[0] = (char) (chars[0] - 32);
        }
        return String.valueOf(chars);
    }

    /**
     * @param value 需要转换的字符串
     * @return
     * @Description :将字符串的首字母转小写
     * @Author :致心
     * @Date :2021/3/12 11:53
     */
    public static String firstLetterToLowercase(String value) {
        // 进行字母的ascii编码前移，效率要高于截取字符串进行转换的操作
        char[] chars = value.toCharArray();
        if (chars[0] >= 'A' && chars[0] <= 'Z') {
            chars[0] = (char) (chars[0] + 32);
        }
        return String.valueOf(chars);
    }

    public static String replaceAllEscape(final String source, final String regex, final String replacement) {
        return source.replaceAll(escapeString(regex), replacement);
    }

    public static String escapeString(String e) {
        StringBuilder result = new StringBuilder();
        Character[] es = new Character[]{'(', '[', '{', '/', '^', '-', '$', '|', '}', ']', ')', '?', '*', '+', '.'};
        List<Character> asList = Arrays.asList(es);
        for (int i = 0; i < e.length(); i++) {
            char c = e.charAt(i);
            if (asList.contains(c)) {
                result.append("\\").append(c);
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }

    /**
     * 转成驼峰
     *
     * @param name
     * @return
     */
    public static String toCamelCase(String name) {
        String pattern = "([a-zA-Z]+)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(name);
        StringBuilder result = new StringBuilder();
        if (m.find()) {
            result = new StringBuilder(firstLetterToLowercase(m.group(1)));
        }
        while (m.find()) {
            String group = m.group(1);
            group = captureName(group);
            result.append(group);
        }
        return result.toString();
    }

    /**
     * 转成大驼峰
     *
     * @param name
     * @return
     */
    public static String toBigCamelCase(String name) {
        return captureName(toCamelCase(name));
    }
}
