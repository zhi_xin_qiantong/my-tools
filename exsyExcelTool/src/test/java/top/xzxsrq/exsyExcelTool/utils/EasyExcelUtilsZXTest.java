package top.xzxsrq.exsyExcelTool.utils;

import org.junit.Test;
import top.xzxsrq.exsyExcelTool.style.entity.ContentCellSetStyleTestVO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class EasyExcelUtilsZXTest {

    @Test
    public void readDataAll() throws IOException {
        InputStream inputStream = new FileInputStream("E:\\window\\desktop\\新建 XLSX 工作表.xlsx");
//        List<ContentCellSetStyleTestVO> contentCellSetStyleTestVOS = EasyExcelUtilsZX.readDataAll(ContentCellSetStyleTestVO.class,
//                inputStream);
//        contentCellSetStyleTestVOS.forEach(System.out::println);
        EasyExcelUtilsZX.readData(ContentCellSetStyleTestVO.class,
                inputStream, (data) -> {
                    System.out.println(data);
                });
    }
}
