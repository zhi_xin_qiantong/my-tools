package top.xzxsrq.exsyExcelTool.style.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ContentCellSetStyleTestVO {
    @ExcelProperty(value = {"名字"})
    private String name;
    @ExcelProperty(value = {"年龄"})
    private BigDecimal age;
}
