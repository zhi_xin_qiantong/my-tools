package top.xzxsrq.exsyExcelTool.style;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.junit.Test;
import top.xzxsrq.common.utils.FileUtilsZX;
import top.xzxsrq.common.utils.ObjectUtilsZX;
import top.xzxsrq.exsyExcelTool.style.entity.ContentCellSetStyleTestVO;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ContentCellSetStyleTest {

    @Test
    public void excelBgColor() throws IOException {
        InputStream classPathFile = FileUtilsZX.getClassPathStream("top.xzxsrq.exsyExcelTool.style/data.json");
        String s = IOUtils.toString(classPathFile, StandardCharsets.UTF_8);
        List<ContentCellSetStyleTestVO> contentCellSetStyleTestVOS = JSON.parseArray(s, ContentCellSetStyleTestVO.class);
        EasyExcel.write("E:\\window\\desktop\\新建 XLSX 工作表.xlsx")
                .head(ContentCellSetStyleTestVO.class)
                .registerWriteHandler(new ContentCellSetStyle<>((cell) -> {
//                        int i = 255;
                    int i = (int) (Math.random() * 244);
                    int i1 = (int) (Math.random() * 244);
//                    System.out.println(i);
//                    System.out.println(i1);
                    cell.setBgColor(new Color(i, 0, i1));
                }, ContentCellSetStyleTestVO.class, contentCellSetStyleTestVOS))
                .sheet()
                .doWrite(contentCellSetStyleTestVOS);
    }

    @Test
    public void XSSFCellStyleEquals() {
        StylesTable stylesTable = new StylesTable(); // 在同一个table下面才能判断到两个单元格样式是否相等
        XSSFCellStyle style1 = new XSSFCellStyle(stylesTable);
        XSSFCellStyle style2 = new XSSFCellStyle(stylesTable);
        XSSFColor xssfColor1 = new XSSFColor(new Color(0, 102, 255), new DefaultIndexedColorMap());
        style1.setFillForegroundColor(xssfColor1);
        XSSFColor xssfColor = new XSSFColor(new Color(255, 0, 0), new DefaultIndexedColorMap());
        style2.setFillForegroundColor(xssfColor);
        System.out.println(style1.hashCode());
        System.out.println(style2.hashCode());
        System.out.println(style1.equals(style2));
    }
}
