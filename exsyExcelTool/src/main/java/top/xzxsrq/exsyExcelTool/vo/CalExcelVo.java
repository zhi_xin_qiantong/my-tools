package top.xzxsrq.exsyExcelTool.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.BooleanEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import top.xzxsrq.common.utils.TimeUiltsZX;
import top.xzxsrq.dynamicObject.EmptyClassZX;
import top.xzxsrq.exsyExcelTool.callback.CalExcelVoContentCallBack;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @program: report-analysis
 * @create: 2021-09-09 14:06
 * 日历vo
 **/
@Data
@ContentStyle(wrapped = BooleanEnum.TRUE, verticalAlignment = VerticalAlignmentEnum.TOP)
public class CalExcelVo implements EmptyClassZX {
    private static final long serialVersionUID = -6492455971996761789L;
    @ColumnWidth(15)
    @ExcelProperty(value = {"一"}, index = 0)
    protected String one;

    @ColumnWidth(15)
    @ExcelProperty(value = {"二"}, index = 1)
    protected String two;

    @ColumnWidth(15)
    @ExcelProperty(value = {"三"}, index = 2)
    protected String three;

    @ColumnWidth(15)
    @ExcelProperty(value = {"四"}, index = 3)
    protected String four;

    @ColumnWidth(15)
    @ExcelProperty(value = {"五"}, index = 4)
    protected String five;

    @ColumnWidth(15)
    @ExcelProperty(value = {"六"}, index = 5)
    protected String six;

    @ColumnWidth(15)
    @ExcelProperty(value = {"日"}, index = 6)
    protected String seven;

    @ExcelIgnore
    private String dayTimeOne; // 记录时间
    @ExcelIgnore
    private String dayTimeTwo; // 记录时间
    @ExcelIgnore
    private String dayTimeThree; // 记录时间
    @ExcelIgnore
    private String dayTimeFour; // 记录时间
    @ExcelIgnore
    private String dayTimeFive; // 记录时间
    @ExcelIgnore
    private String dayTimeSix; // 记录时间
    @ExcelIgnore
    private String dayTimeSeven; // 记录时间

    @ExcelIgnore
    private int contentType; // 1 显示月份头部 2 显示周 3 是内容区域

    @ExcelIgnore
    private String month; // 月份

    @ExcelIgnore
    public static Integer monthHead = 1;
    @ExcelIgnore
    public static Integer showWeek = 2;
    @ExcelIgnore
    public static Integer contentArea = 3;
    @ExcelIgnore
    public static String[] fileds = new String[]{"one", "two", "three", "four", "five", "six", "seven"};
    @ExcelIgnore
    public static String[] dayFileds = new String[]{"dayTimeOne", "dayTimeTwo", "dayTimeThree", "dayTimeFour", "dayTimeFive", "dayTimeSix", "dayTimeSeven"};
    @ExcelIgnore
    public static String wrap = String.valueOf((char) 10);


    /**
     * 生成日历
     *
     * @param month
     * @return
     */
    public static List<CalExcelVo> generateCalendar(String month) {
        CalExcelVo[] list = new CalExcelVo[9];
        for (int i = 0; i < list.length; i++) {
            list[i] = new CalExcelVo();
            list[i].setMonth(month);
        }
        Date date = TimeUiltsZX.convert2FormatDate(month, "yyyy-MM");
        String formatString = TimeUiltsZX.convert2FormatString(date, "yyyy年MM月");
        list[0].setOne(formatString);
        list[0].setContentType(CalExcelVo.monthHead);
        list[1].setOne("一");
        list[1].setTwo("二");
        list[1].setThree("三");
        list[1].setFour("四");
        list[1].setFive("五");
        list[1].setSix("六");
        list[1].setSeven("日");
        list[1].setContentType(CalExcelVo.showWeek);
        int days = TimeUiltsZX.getDayOfMonth(date);
        int count = 1;
        int week = TimeUiltsZX.getWeek(month + "-01"); // 获取月初是星期几
        week--;
        if (week == 0) {
            week = 7;
        }
        for (int i = 2; i < list.length - 1; i++) {
            list[i].setContentType(CalExcelVo.contentArea);
            for (int i1 = week - 1; i1 < CalExcelVo.fileds.length; i1++) {
                if (count > days) {
                    break;
                }
                String filed = CalExcelVo.fileds[i1];
                String dayFiled = CalExcelVo.dayFileds[i1];
                list[i].setValue(filed, count < 10 ? "0" + count : count + "");
                list[i].setValue(dayFiled, count < 10 ? month + "-0" + count : month + "-" + count);
                count++;
            }
            week = 1;
        }
        return Arrays.asList(list);
    }

    public static void contentForEach(List<CalExcelVo> calExcelVos, CalExcelVoContentCallBack callBack) {
        calExcelVos.forEach(j -> {
            if (j.getContentType() != CalExcelVo.contentArea) {
                return;
            }
            for (int i1 = 0; i1 < CalExcelVo.dayFileds.length; i1++) {
                String dayFiled = CalExcelVo.dayFileds[i1];
                String filed = CalExcelVo.fileds[i1];
                String time = j.getValue(dayFiled);
                if (StringUtils.isNotEmpty(time)) {
                    callBack.getTime(j, time, filed);
                }
            }
        });
    }
}
