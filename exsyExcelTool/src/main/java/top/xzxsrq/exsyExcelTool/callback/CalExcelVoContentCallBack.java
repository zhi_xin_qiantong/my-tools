package top.xzxsrq.exsyExcelTool.callback;

import top.xzxsrq.exsyExcelTool.vo.CalExcelVo;

/**
 * 日历遍历时间的回调函数
 *
 * @program: report-analysis
 * @create: 2021-09-10 14:12
 **/
@FunctionalInterface
public interface CalExcelVoContentCallBack {
    void getTime(CalExcelVo item, String time, String fileName);
}
