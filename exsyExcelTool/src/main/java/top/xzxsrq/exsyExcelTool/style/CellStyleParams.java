package top.xzxsrq.exsyExcelTool.style;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import lombok.Data;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * 单元格样式参数
 *
 * @program: MyUtils
 * @create: 2021-11-12 11:20
 **/
@Data
public class CellStyleParams<T> {
    private int rowIndex;
    private int columnIndex;
    private T row;
    private Cell cell;
    private Head head;
    private Workbook workbook;
    private WriteCellData<?> writeCellData;
    private DataFormat dataFormat;
    private Map<Integer, XSSFCellStyle> cellStyleCache = new HashMap<>(); // 缓存一下 一个表格最多创建6W个样式，使用hashCode作为key

    public void setWorkbook(Workbook workbook) {
        this.workbook = workbook;
        dataFormat = workbook.createDataFormat();
    }


    private static <T> void changeCellStyle(XSSFCellStyle cellStyle, CellStyleParams<T> cellStyleParams) {
        XSSFCellStyle xssfCellStyle = cellStyleParams.cellStyleCache.get(cellStyle.hashCode());
        if (xssfCellStyle != null) {
//            System.out.println("命中缓存");
            cellStyleParams.cell.setCellStyle(xssfCellStyle);
            cellStyleParams.writeCellData.setWriteCellStyle(null);
        } else {
            XSSFCellStyle cloneCellStyle = (XSSFCellStyle) cellStyleParams.cloneCellStyle();
            cellStyleParams.cell.setCellStyle(cloneCellStyle);
            cellStyleParams.writeCellData.setWriteCellStyle(null);
            cellStyleParams.cellStyleCache.put(cloneCellStyle.hashCode(), cloneCellStyle);
        }
    }

    /**
     * 设置背景颜色 <br/>
     * setBgColor(new java.awt.Color(int,int,int))
     *
     * @param apply 颜色
     */
    public void setBgColor(Color apply) {
        // 使用easyexcel的样式重写
//        WriteCellStyle orCreateStyle = writeCellData.getOrCreateStyle();
//        orCreateStyle.setFillForegroundColor(IndexedColors.RED.getIndex()); // 10
//        orCreateStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
        // 使用poi的重写
        XSSFColor xssfColor = new XSSFColor(apply, new DefaultIndexedColorMap());
        XSSFCellStyle cellStyle = (XSSFCellStyle) cell.getCellStyle();
        cellStyle.setFillForegroundColor(xssfColor);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        CellStyleParams.changeCellStyle(cellStyle, this);
    }

    /**
     * 设置背景颜色
     */
    public void setBgColorOfWhite() {
        setBgColor(Color.white);
    }

    /**
     * 设置千分符
     */
    public void setThousandths() {
        XSSFCellStyle cellStyle = (XSSFCellStyle) cell.getCellStyle();
        // 设置千位分隔符
        cellStyle.setDataFormat(dataFormat.getFormat("#,##0"));
        cell.setCellType(CellType.NUMERIC);
        changeCellStyle(cellStyle,this);
    }

    /**
     * 设置百分比
     */
    public void setPer() {
        XSSFCellStyle cellStyle = (XSSFCellStyle) cell.getCellStyle();
        // 设置百分比
        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        cell.setCellType(CellType.NUMERIC);
        changeCellStyle(cellStyle,this);
    }

    public CellStyle cloneCellStyle() {
        CellStyle colStyle = cell.getCellStyle();
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.cloneStyleFrom(colStyle);
        return cellStyle;
    }
}
