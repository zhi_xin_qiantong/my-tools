package top.xzxsrq.exsyExcelTool.mergeHead;

import com.alibaba.excel.annotation.ExcelProperty;
import javassist.CtField;
import javassist.bytecode.AnnotationsAttribute;
import lombok.Data;
import top.xzxsrq.dynamicObject.AllUseCoreClassZX;
import top.xzxsrq.dynamicObject.DynamicUtilsZX;

/**
 * 合并表头
 *
 * @program: MyUtils
 * @create: 2021-09-30 19:07
 **/
@Data
public class MergeHead {

    public static AllUseCoreClassZX mergeHead(AllUseCoreClassZX target, AllUseCoreClassZX source) throws Exception {
        AllUseCoreClassZX emptyClass = DynamicUtilsZX.mergeObject(target, source);
        CtField[] declaredFields = emptyClass.getObject().getDeclaredFields();
        int count = 0;
        for (CtField declaredField : declaredFields) {
            if ("serialVersionUID".equals(declaredField.getName())) {
                continue;
            }
            AnnotationsAttribute annotationsAttribute = DynamicUtilsZX.fieldGetAnnotationsAttribute(declaredField);
            DynamicUtilsZX.annotationUpdateValue(declaredField, annotationsAttribute, ExcelProperty.class, "index", ++count);
        }
        return emptyClass;
    }


    private MergeHead() {
    }
}
