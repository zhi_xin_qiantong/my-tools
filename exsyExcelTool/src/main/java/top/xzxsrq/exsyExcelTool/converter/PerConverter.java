package top.xzxsrq.exsyExcelTool.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import top.xzxsrq.common.utils.NumberUtilsZX;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * excel百分比格式化
 *
 * @program: report-analysis
 * @create: 2021-07-30 16:22
 **/
public class PerConverter implements Converter<BigDecimal> {
    @Override
    public Class supportJavaTypeKey() {
        return BigDecimal.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.NUMBER;
    }

    @Override
    public WriteCellData<BigDecimal> convertToExcelData(BigDecimal value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        BigDecimal mul = NumberUtilsZX.mul(value, 100);
        NumberFormat instance = NumberFormat.getInstance();
        String format = instance.format(mul);
        return new WriteCellData(format + '%');
    }
}
