package top.xzxsrq.exsyExcelTool.ReadListener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;

import java.util.function.Consumer;

public class ObjectReadListener<T> implements ReadListener<T> {
    private final Consumer<T> callback;


    public ObjectReadListener(Consumer<T> callback) {
        this.callback = callback;
    }


    @Override
    public void invoke(T data, AnalysisContext context) {
        callback.accept(data);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
