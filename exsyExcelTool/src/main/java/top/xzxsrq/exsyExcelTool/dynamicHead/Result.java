package top.xzxsrq.exsyExcelTool.dynamicHead;

import lombok.Data;
import top.xzxsrq.dynamicObject.EmptyClassZX;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 返回的结果
 *
 * @program: MyUtils
 * @create: 2021-09-22 13:40
 **/
@Data
public class Result {
    private List<String> fields; // 不能使用set, 会hash排序
    private Class<EmptyClassZX> headClass;

    public void setFields(List<String> fields) {
        this.fields = fields.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
