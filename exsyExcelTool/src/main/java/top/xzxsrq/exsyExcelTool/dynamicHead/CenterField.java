package top.xzxsrq.exsyExcelTool.dynamicHead;

import lombok.Data;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 中间结果
 *
 * @program: MyUtils
 * @create: 2021-09-22 13:59
 **/
@Data
public class CenterField {
    private String field; // 字段 如果有子项就取子项的
    private List<String> labels = new LinkedList<>();
    private Integer columnWidth; // 宽度 如果有子项就取子项的
    private Align align = Align.center;
}
