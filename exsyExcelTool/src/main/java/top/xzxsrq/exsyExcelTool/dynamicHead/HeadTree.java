package top.xzxsrq.exsyExcelTool.dynamicHead;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import javassist.CtField;
import javassist.bytecode.annotation.Annotation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import top.xzxsrq.dynamicObject.AllUseCoreClassZX;
import top.xzxsrq.dynamicObject.DynamicUtilsZX;
import top.xzxsrq.dynamicObject.EmptyClassZX;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 动态表头生成的对象里面一定是字符串的字段
 *
 * @program: MyUtils
 * @create: 2021-09-22 13:35
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class HeadTree extends CenterField {
    private String label; // 头部显示 如果有子项就进行拼接
    private List<HeadTree> children; // 子项

    /**
     * 有子项就取子项的
     *
     * @param head
     * @return
     */
    public static Result getHead(List<HeadTree> head) {
        Result result = new Result();
        if (CollectionUtils.isEmpty(head)) {
            return null;
        }
        List<CenterField> centerFields = new LinkedList<>();
        head.forEach(i -> {
            if (CollectionUtils.isEmpty(i.getChildren())) {
                centerFields.add(getCenterFieldItem(i));
            } else {
                centerFields.addAll(getHead(i, i.getChildren(), new LinkedList<>()));
            }
        });
        List<String> collect = centerFields.stream()
                .map(CenterField::getField)
                .distinct()
                .collect(Collectors.toList());
        result.setFields(collect);
        try {
            AllUseCoreClassZX emptyClass = AllUseCoreClassZX.getEmptyClass();
            AtomicInteger index = new AtomicInteger();
            for (String s : result.getFields()) {
                CtField ctField = DynamicUtilsZX.makeFieldAndGetAndSet(emptyClass, s);
                centerFields.stream()
                        .filter(i -> s.equals(i.getField()))
                        .findFirst()
                        .ifPresent(i -> {
                            List<Annotation> annotations = new LinkedList<>();
                            Annotation annotation = DynamicUtilsZX.makeAnnotation(ExcelProperty.class, emptyClass);
                            DynamicUtilsZX.annotationAddValue(annotation, emptyClass, "value", i.getLabels().toArray(new String[0]));
                            DynamicUtilsZX.annotationAddValue(annotation, emptyClass, "index", index.getAndIncrement());
                            annotations.add(annotation);
                            if (i.getColumnWidth() != null) {
                                Annotation annotation1 = DynamicUtilsZX.makeAnnotation(ColumnWidth.class, emptyClass);
                                DynamicUtilsZX.annotationAddValue(annotation1, emptyClass, "value", i.getColumnWidth());
                                annotations.add(annotation1);
                            }
                            if (i.getAlign() != null) {
                                Annotation annotation1 = DynamicUtilsZX.makeAnnotation(ContentStyle.class, emptyClass);
                                DynamicUtilsZX.annotationAddValue(annotation1, emptyClass, "horizontalAlignment", Align.get(i.getAlign()));
                                annotations.add(annotation1);
                                Annotation annotation2 = DynamicUtilsZX.makeAnnotation(HeadStyle.class, emptyClass);
                                DynamicUtilsZX.annotationAddValue(annotation2, emptyClass, "horizontalAlignment", Align.get(i.getAlign()));
                                annotations.add(annotation2);
                            }
                            DynamicUtilsZX.fieldAddAnnotation(ctField, annotations);
                        });
            }
            DynamicUtilsZX.makeToString(emptyClass, result.getFields());
            Class<EmptyClassZX> cClass = emptyClass.getCClass();
            result.setHeadClass(cClass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static CenterField getCenterFieldItem(HeadTree i) {
        CenterField item = new CenterField();
        String field = i.getField();
        item.setField(field);
        item.setColumnWidth(i.getColumnWidth());
        List<String> label = new LinkedList<>();
        label.add(i.getLabel());
        item.setLabels(label);
        item.setAlign(i.getAlign());
        return item;
    }

    private static List<CenterField> getHead(HeadTree parent, List<HeadTree> children, List<String> parentLables) {
        List<CenterField> centerFields = new LinkedList<>();
        children.forEach(i -> {
            if (CollectionUtils.isEmpty(i.getChildren())) {
                CenterField centerFieldItem = getCenterFieldItem(i);
                centerFieldItem.getLabels().add(0, parent.getLabel());
                centerFieldItem.getLabels().addAll(0, parentLables);
                centerFields.add(centerFieldItem);
            } else {
                List<String> news = new LinkedList<>(parentLables);
                news.add(parent.getLabel());
                centerFields.addAll(getHead(i, i.getChildren(), news));
            }
        });
        return centerFields;
    }
}
