package top.xzxsrq.exsyExcelTool.dynamicHead;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * 对齐方式
 *
 * @program: MyUtils
 * @create: 2021-09-24 10:47
 **/
@Getter
@AllArgsConstructor
public enum Align {
    left("左对齐"),
    right("右对齐"),
    center("居中");
    private String name;

    public static HorizontalAlignment get(Align align) {
        if (align == Align.center) {
            return HorizontalAlignment.CENTER;
        } else if (align == Align.left) {
            return HorizontalAlignment.LEFT;
        } else if (align == Align.right) {
            return HorizontalAlignment.RIGHT;
        } else {
            return HorizontalAlignment.GENERAL;
        }
    }
}
