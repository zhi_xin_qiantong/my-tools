package top.xzxsrq.dynamicObject.MyMemberValueVisitor;

import javassist.bytecode.annotation.*;
import lombok.Data;
import top.xzxsrq.common.utils.StringUtilsZX;
import top.xzxsrq.dynamicObject.AllUseCoreClassZX;
import top.xzxsrq.dynamicObject.DynamicUtilsZX;

import java.util.Map;
import java.util.function.Consumer;

/**
 * 值替换类
 *
 * @program: MyUtils
 * @create: 2021-09-26 19:28
 **/
@Data
public class ReplaceMemberValueVisitorZX implements MemberValueVisitor {
    private String name;
    private String plaEnd;
    private String plaBegin;
    private Map<String, String> repalce;
    private AllUseCoreClassZX emptyClass;
    private Consumer<Exception> callBack = Throwable::printStackTrace;

    public ReplaceMemberValueVisitorZX(AllUseCoreClassZX emptyClass, String name, Map<String, String> repalce, String plaBegin, String plaEnd) {
        this.emptyClass = emptyClass;
        this.repalce = repalce;
        this.plaBegin = plaBegin;
        this.plaEnd = plaEnd;
        this.name = name;
    }

    public ReplaceMemberValueVisitorZX(AllUseCoreClassZX emptyClass, String name, Map<String, String> repalce, String plaBegin, String plaEnd, Consumer<Exception> callBack) {
        this.plaEnd = plaEnd;
        this.plaBegin = plaBegin;
        this.repalce = repalce;
        this.emptyClass = emptyClass;
        this.callBack = callBack;
        this.name = name;
    }

    @Override
    public void visitAnnotationMemberValue(AnnotationMemberValue node) { // 不知道替换什么
        Annotation value = node.getValue();
        DynamicUtilsZX.placeholderReplacement(emptyClass, value, repalce, plaBegin, plaEnd);
    }

    @Override
    public void visitArrayMemberValue(ArrayMemberValue node) {
        MemberValue[] value = node.getValue();
        for (MemberValue memberValue : value) {
            memberValue.accept(this);
        }
    }

    @Override
    public void visitBooleanMemberValue(BooleanMemberValue node) {

    }

    @Override
    public void visitByteMemberValue(ByteMemberValue node) {

    }

    @Override
    public void visitCharMemberValue(CharMemberValue node) {

    }

    @Override
    public void visitDoubleMemberValue(DoubleMemberValue node) {

    }

    @Override
    public void visitEnumMemberValue(EnumMemberValue node) {

    }

    @Override
    public void visitFloatMemberValue(FloatMemberValue node) {

    }

    @Override
    public void visitIntegerMemberValue(IntegerMemberValue node) {

    }

    @Override
    public void visitLongMemberValue(LongMemberValue node) {

    }

    @Override
    public void visitShortMemberValue(ShortMemberValue node) {

    }

    @Override
    public void visitStringMemberValue(StringMemberValue node) {
        String value = node.getValue();
        value = ReplaceMemberValueVisitorZX.repalceMap(repalce, plaBegin, plaEnd, value);
        node.setValue(value);
    }

    @Override
    public void visitClassMemberValue(ClassMemberValue node) {

    }

    /**
     *
     * @param repalce
     * @param plaBegin
     * @param plaEnd
     * @param replaceValue 要替换的值
     * @return
     */
    public static String repalceMap(Map<String, String> repalce, String plaBegin, String plaEnd, String replaceValue) {
        for (String key : repalce.keySet()) {
            String value = repalce.get(key);
            replaceValue = StringUtilsZX.replaceAllEscape(replaceValue, plaBegin + key + plaEnd, value);
        }
        return replaceValue;
    }
}
