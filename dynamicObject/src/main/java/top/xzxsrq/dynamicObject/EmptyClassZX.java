package top.xzxsrq.dynamicObject;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * @program: MyUtils
 * @create: 2021-08-25 15:16
 **/
public interface EmptyClassZX extends Serializable {
    default String getValue(String fieldName) {
        return this.getValue(fieldName, String.class);
    }

    /**
     * @param fieldName
     * @param tClass    这个用于指定泛型类型
     * @param <T>
     * @return
     */
    default <T> T getValue(String fieldName, Class<T> tClass) {
        try {
            Field declaredField = this.getClass().getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            return (T) declaredField.get(this);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    default void setValue(String fieldName, Object value) {
        try {
            Field declaredField = this.getClass().getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            declaredField.set(this, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    default void setValue(String fieldName, String value) {
        this.setValue(fieldName, (Object) value);
    }
}
